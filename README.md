# Modelling Course

Course material for: 
"Modelling and Data Analysis for Pharmaceutical Sciences: from Molecules to Clinical Practice"

## Table of contents
1. PDB visualisation
    - Learn how to use PyMol to explore the 3D structure of a protein
2. Homology modelling
    - Use the SWISSMODELER to construct a homology model of the kinase domain of EGFR 
3. Docking
    - Dock imatinib to the WT and T315I Abl kinase to understand the structural implications of mutations for drug resistance
4. MD simulations
    - Set up, run, and analyse a protein-ligand simulation through Google.Colab


